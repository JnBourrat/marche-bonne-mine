---
title: Ce samedi 11 Juin 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, cerises, porc fermier
description: Découvrez les producteurs présents sur place en ce samedi 11 juin ! Et une idée recette pour le barbecue ! [...]
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 06/08/2022
---

**Retrouvez nos différents produits afin de réussir votre barbecue de dimanche :**

- Le **porc fermier** de chez **Terre d'Arjoux** ! La vedette du barbecue c'est quand même la saucisse !

- Les pains d'**O Pain d'Autrefois** ! 

- Les fromages de chèvre de **Laura et Mickaël (La Chèvre de Milo)** ! Un fromage de chèvre mi-sec, du sel du poivre, de l'huile d'olive, enroulé dans du papier d'alu et hop ! Chèvre chaud au barbecue ! 

- Les **œufs bio** d'**Angélique (Zeubio)** !

- Des **champignons** du côté de **Paul** **(ChampiMonts)** ! Des grandes pleurotes, la face striée imbibée d'huile de tournesol, de la fleur de sel et bim ! 5 minutes sur la face striée, 2 minutes sur la face lisse et vous aurez d'excellentes pleurotes grillées au barbecue !

- Le **miel** de **Cécile** **(Au Rythme des Abeilles)** !

- Les **légumes** et **aromatiques** de **La Ferme du Petit Arbre** ! Pour accompagner votre barbecue : des aromatiques, des salades, des radis et des courgettes à griller !

- Les cerises de Claire **(La Terre Mère)** feront sûrement leur dernier passage par le marché, profitez-en ! 
 
--- 
<br/>

## L'instant recette
<br/>
Cette semaine une petite idée de recette rabiotée sur les internets mondiaux par Paul !
Du fromage de chèvre, du miel et des champignons à faire au barbecue ou au four : les <a href="https://www.colruyt.be/fr/en-cuisine/recette/champignons-farcis-au-chevre" target="blank">champignons farcis au chèvre et au miel</a> !

<br/>
<br/>

---
<br/>

## Quelques informations pêle-mêle
<br/>

- Ça y est ! Le légume star de l'été arrive ! Tout le monde les attend depuis des mois et des mois ! Prières et souhaits ont été entendus par votre maraîcher favori et il les amène en grande pompe samedi ! Oui, je parle bien du **FENOUIL** ! Non, je ne parle pas des tomates mais ne vous inquiétez pas, elles pointeront bientôt le bout de leur pédoncule. Patience ! Et puis le fenouil c'est aussi très bon au barbecue.
 
- Si vous l'aviez raté la semaine dernière, nous vous rappelons qu'un **tiers-lieu culturel et social** est en cours de création dans l'**ancienne caserne des pompiers** de Sourcieux-les-Mines ! 
Si vous souhaitez en savoir plus, rendez-vous sur leur page Facebook en suivant <a href="https://www.facebook.com/lacasernesourcieux" target="\_blank">ce lien</a> !

<br/>

---
<br/>

## ET À SAMEDI !