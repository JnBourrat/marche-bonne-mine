---
title: Février 2025 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, porc fermier
description: « Que des Cœurs » pour ce début d'année 2025, retrouvez le calendrier !
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: 2502-apercu.png
date: 01/31/2025
---
<br/>

<img class="planning-gazette aligne-a-droite"
   src="/2502-numerique.png" 
   alt="Planning de février 2025 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69). 142, route du gervais, 69210 Sourcieux-les-Mines.
          En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny), Champignons bio & soupes - Champimonts du Lyonnais (AB, Villechenève).
          En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs bio - Zeubio (AB, Saint-Julien-sur-Bibost), Miel bio - Au rythme des abeilles (AB, Châtillon).
          + LE SAMEDI 15 FÉVRIER, 3ÈME SAMEDI DU MOIS CONSACRÉ À L'ARTISANAT : Affûteur itinérant - FX Chastang, Saint-Nectaire fermier bio - GAEC des Moussages.
          + LE SAMEDI 22 FÉVRIER  DERNIER SAMEDI DU MOIS - GRAND MARCHÉ : Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny) ; Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost) ; Œufs, savons - Zeubio (AB, Saint-Julien-sur-Bibost) ; Fruits, jus - Le verger des Presles (AB, Pollionay) ; Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères)."
   title="Planning février 2025 au Marché Bonne Mine">
<br/>
