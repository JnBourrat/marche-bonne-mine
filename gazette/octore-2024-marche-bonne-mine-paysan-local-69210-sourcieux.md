---
title: Octobre 2024 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, paysan, biologique, paysan, fromage, légumes, champignons, miel, oeufs, pain, porc fermier
description: Le mois d'octobre est arrivé au Marché Bonne Mine ! Au programme de ce mois, du fro...
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: 2410-apercu.png
date: 09/01/2024
---
<br/>
<img class="planning-gazette aligne-a-droite"
   src="/2410-numerique.png"
   altText="Planning d'octobre 2024 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69). 142, route du gervais, 69210 Sourcieux-les-Mines. En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny), Champignons bio & soupes - Champimonts du Lyonnais (AB, Villechenève). En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs bio - Zeubio (AB, Saint-Julien-sur-Bibost), Miel bio - Au rythme des abeilles (AB, Châtillon). + LE SAMEDI 12 OCTOBRE, Saint-Nectaire fermier - GAEC Les Moussages. + LE SAMEDI 19 OCTOBRE, 3ÈME SAMEDI DU MOIS CONSACRÉ À L'ARTISANAT : Affûteur itinérant - FX Chastang, Réparation de vélo sur place - SW Cycles. + LE SAMEDI 26 OCTOBRE, DERNIER SAMEDI DU MOIS - GRAND MARCHÉ : Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères), Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny), Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost), Fruits bio & jus - Le verger des Presles, Oeufs, savons - Zeubio."
   title="Planning du mois d'octobre 2024 au Marché Bonne Mine (Sourcieux-les-Mines).">
<br/>
