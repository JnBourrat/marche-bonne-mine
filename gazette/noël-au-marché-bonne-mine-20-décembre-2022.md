---
title: Noël au Marché Bonne Mine - Mardi 20 Décembre 2022 de 16h à 20h
category: marché, marché de noël, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, porc fermier, fête, vin chaud, bière chaude, jus de pomme chaud, buvette
description: Le père Noël s'invite au Marché Bonne Mine le 20 décembre, de 16h à 20h. Au program[...]
tags: marché de Noël, Sourcieux-les-Mines, Marché Bonne Mine, Rhône
image: aperçu-affiche-noel-mbm.jpg
date: 11/24/2022
---
<br/>
<img class="affiche-evenement" src="/affiche-noel-mbm.jpg" alt="affiche noël au MBM le 20 décembre 2022" title="affiche marché de noël">
<br/>

### RÉSERVEZ VOTRE MARDI 20 DÉCEMBRE DE 16H à 20H POUR UNE BONNE DOSE DE MAGIE DE NOËL

<br>

- Plus de 20 artisans, paysans et associations (plus d'informations dans les semaines à venir) ! 

- Des animations en veux-tu en voilà : 
   - Visite du Père-Noël
   - Vin chaud, bière chaude, jus de pomme chaud
   - Des chants de Noël
   - Emballages cadeaux éco-responsables
   - Frichti de Noël : soupe, crêpes...

<br>
Un marché de Noël convivial à Sourcieux-les-Mines au Marché Bonne Mine !
