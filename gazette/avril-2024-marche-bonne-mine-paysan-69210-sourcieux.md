---
title: Avril 2024 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, paysan, biologique, paysan, fromage, légumes, champignons, miel, oeufs, pain, porc fermier
description: Le printemps s'installe avec le mois d'Avril au Marché Bonne Mine. Au programme de ce mois, encore pleins de bons produits locau...
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: 2404-apercu.png
date: 04/01/2024
---
<br/>
<img class="planning-gazette aligne-a-droite"
   src="/2404-numerique.png"
   altText="Planning d'avril 2024 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69).
   142, route du gervais, 69210 Sourcieux-les-Mines.
   En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny), Champignons bio & soupes - Champimonts du Lyonnais (AB, Villechenève). En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs bio - Zeubio (AB, Saint-Julien-sur-Bibost), Miel bio - Au rythme des abeilles (AB, Châtillon).
   + LE SAMEDI 13 AVRIL : Saint-Nectaire fermier bio - GAEC des Moussages.
   + LE SAMEDI 20 AVRIL, 3ÈME SAMEDI DU MOIS CONSACRÉ À L'ARTISANAT : Affûteur itinérant - FX Chastang.
   + LE SAMEDI 27 AVRIL, DERNIER SAMEDI DU MOIS - GRAND MARCHÉ : Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères), Les Serres de Ripan - Pépinière de plantes potagères & aromatiques, Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny), Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost), Œufs, savons - Zeubio (AB, Saint-Julien-sur-Bibost), Confitures & gelées - Les cueillettes d'Eddy (récoltes sauvages), Fruits bio & jus - Le verger des Presles, Bières artisanales - Prisca.
   + PORTES OUVERTES DE FERME EN FERME LES 27 & 28 AVRIL DE 10H À 18H !"
   title="Planning du mois d'avril 2024 au Marché Bonne Mine (Sourcieux-les-Mines).">
<br/>
