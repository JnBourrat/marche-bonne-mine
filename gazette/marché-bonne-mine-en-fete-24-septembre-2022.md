---
title: Marché Bonne Mine en fête le 24 Septembre 2022
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, cerises, porc fermier, fête, visite de ferme, conte, repas, bal folk, buvette
description: Venez faire la fête au Marché Bonne Mine le 24 Septembre ! Au programme marché, visite de ferme, conte, repas, bal folk & buvette ! Réservez votre repas [...]
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan, évènement, visite de ferme, conte, repas, bal folk
date: 08/24/2022
---
La rentrée sera marquée cette année par l'organisation d'une belle journée sous le signe de la fête et de la culture au Marché Bonne Mine ! Nous vous avons pour cela concocté une journée aux petits oignons le samedi 24 Septembre ! Au programme :

<br/>
<img src="/programmee_24-09.png" alt="programme du 24 septeembre" title="prorgramme" height="auto">
<br/>

**9h00 - 12h00 :** le Marché se tiendra au mêmes horaires que d'habitude, mais cette fois-ci avec de chouettes artisans du coin ! De plus amples détails arriveront dans les semaines à venir !
 
**14h00 - 18h00 :** des visites de fermes seront organisées l'après-midi entre Champimonts (Paul) à Villechenève, Zeubio (Angélique) et Terre d'Arjoux (Benoît et Olivier) à Saint-Julien-sur-Bibost. Venez découvrir l'envers du décor et le quotidien de vos producteurs !

<br/>
<img src="/visites_24-09.png" alt="programme du 24 septeembre" title="prorgramme" height="auto">
<br/>

**19h00 :** <a href="https://www.facebook.com/profile.php?id=100076333602241" target="blank">« Au bout du doigt »</a> - Conte et théâtre chanté, dans lequel on suivra les aventures de Pépé, paysan Bressan ayant perdu un... bout de doigt ! 
 
**20h00 :** Repas & buvette, avec le concours de l'incomparable <a href="https://www.facebook.com/BrasseriePrisca/" target=blank>brasserie Prisca</a> !
Le repas paysan sera confectionné à partir des produits du marché et se composera d'une entrée, d'un plat, de fromage et d'un dessert. Le prix du repas a été fixé à 10€ pour les menus enfant et végétarien et 12€ pour le repas avec le jambon à la broche de Terre d'Arjoux !
Afin de pouvoir nous permettre d'anticiper les quantités, le repas sera uniquement sur réservation (suivez <a href="https://www.marchebonnemine.fr/fete" target="blank">ce lien</a>). Sans réservation, personne ne vous empêchera de boire un jus de fruit ou une petite mousse.
<span style="color:red">**ATTENTION : vous êtes extrêmement nombreux à vous être manifestés pour commander un repas et nous vous en remercions ! Malheureusement, nous n'avons pas encore les moyens humains et logistiques pour faire face à un tel nombre de réservations et nous nous voyons donc contraints à interrompre la réservation de repas...**</span>


 
**21h00 - 22h30 :** pour clore la journée en musique, nous accueillerons <a href="https://www.facebook.com/DuoCailini/" target="blank">Cailini</a>, duo Folk composé de Sandrine Saliba (accordéon) et Estelle Avondo (alto), qui vous fera danser au gré de rythmes folk. Avec une initiation aux danses traditionnelles pour celles et ceux qui ne connaissent pas les pas, bien entendu !
