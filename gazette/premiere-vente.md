---
title: Inauguration du Marché Bonne Mine le samedi 23 Avril 2022
category: marché, vente
description: Retrouvez-nous samedi 23 Avril 2022 à partir de 9h pour l'inauguration du Marché Bonne Mine au Gaec La Chèvre de Milo, 142 Route du Gervais à Sourcieux-les-Mines ! Marché de producteurs locaux, convivial et 100% paysan ! Porc fermier, légumes, fromages, oeufs, miel, champignons, pain et vin trouveront confort dans votre panier !
tags: marché, direct producteur, vente
date: 04/02/2022
---

Retrouvez-nous samedi **23 Avril 2022 à partir de 9h** pour le premier Marché Bonne Mine au Gaec La Chèvre de Milo, 142 Route du Gervais à Sourcieux-les-Mines !

Le Marché Bonne Mine est un rassemblement de producteurs et paysans locaux ! Légumes, fromages, porc fermier, champignons, miel, oeufs, pain et vin trouveront confort dans votre panier avant de rejoindre vos assiettes, verres et autres contenants dédiés à la dégustation et à la gourmandise !

Vous pourrez trouver plus d'informations sur les producteurs et leurs produits sur la **[page des producteurs](https://www.marchebonnemine.fr/les-producteurs)** et sur la **<a href="https://www.facebook.com/March%C3%A9-Bonne-Mine-108283768472759/" target="\_blank">page facebook</a>** !

À très vite !
