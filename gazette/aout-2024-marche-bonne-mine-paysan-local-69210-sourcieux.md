---
title: Août 2024 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, paysan, biologique, paysan, fromage, légumes, champignons, miel, oeufs, pain, porc fermier
description: Le mois d'août est arrivé au Marché Bonne Mine et avec lui le traditionnel bal folk ! Au programme de ce mois, encor...
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: 2408-apercu.png
date: 07/01/2024
---
<br/>
<img class="planning-gazette aligne-a-droite"
   src="/2408-numerique.png"
   altText="Planning d'août 2024 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69).
   142, route du gervais, 69210 Sourcieux-les-Mines.
   En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny). En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs - Aux p'tits plaisirs des champs (HVE, Savigny), Miel bio - Au rythme des abeilles (AB, Châtillon).
   + PAS DE MARCHÉ LE SAMEDI 17 AOÛT
   + LE SAMEDI 31 JUILLET, DERNIER SAMEDI DU MOIS - GRAND MARCHÉ LE MATIN & BAL FOLK à 19h : Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères), Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny), Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost), Fruits bio & jus - Le verger des Presles."
   title="Planning du mois d'août 2024 au Marché Bonne Mine (Sourcieux-les-Mines).">
<br/>
