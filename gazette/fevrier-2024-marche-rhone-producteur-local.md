---
title: Février 2024 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, paysan, biologique, paysan, fromage, légumes, champignons, miel, oeufs, pain, porc fermier
description: L'année 2024 continue avec un joli mois de février au Marché Bonne Mine. Au programme de ce mois, encore pleins de bons produits locaux (si vous êtes des environs) !
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: apercu-planning-2402.png
date: 02/01/2024
---
<br/>

<img class="planning-gazette aligne-a-droite"
   src="/planning-complet-2402.png" 
   altText="Planning de février 2024 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69). 142, route du gervais, 69210 Sourcieux-les-Mines. En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny), Champignons bio & soupes - Champimonts du Lyonnais (AB, Villechenève). En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs bio - Zeubio (AB, Saint-Julien-sur-Bibost), Miel bio - Au rythme des abeilles (AB, Châtillon). + LE SAMEDI 3 FÉVRIER : lapins et volailles en direct de la ferme - Aux p'tits plaisir des champs (Savigny). + LE SAMEDI 17 FÉVRIER, 3ÈME SAMEDI DU MOIS CONSACRÉ À L'ARTISANAT : Affûteur itinérant - FX Chastang, Réparation de vélo sur place - SW Cycles (Saint-Pierre-la-Palud), Saint-Nectaire fermier bio - GAEC des Moussages, , Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères). + LE SAMEDI 24 FÉVRIER  DERNIER SAMEDI DU MOIS - GRAND MARCHÉ : Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny), Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost), Œufs, savons - Zeubio (AB, Saint-Julien-sur-Bibost), Fruits, jus - Le verger des Presles (AB, Pollionay). + PLEIN D’AUTRES SURPRISES : bières artisanales, confitures et gelées issues de récoltes sauvages, etc !"
   title="Planning du mois de février 2024 au Marché Bonne Mine (Sourcieux-les-Mines).">
<br/>
