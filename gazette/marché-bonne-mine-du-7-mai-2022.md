---
title: Ce samedi 7 Mai 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, charte, paysan, fromage, légumes, maraîchage,  champignons, miel, oeufs, pain
description: Retrouvez-nous samedi 7 mai 2022 à partir de 9h pour le Marché Bonne Mine au Gaec La Chèvre de Milo, 142 Route du Gervais à Sourcieux-les-Mines ! Marché de producteurs locaux, convivial et 100% paysan ! Cliquez sur l'article afin d'avoir plus d'informations sur les producteurs présents au marché ce samedi !
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 05/04/2022
---

**Votre panier - si vous ne l'oubliez pas à la maison - pourra acceuillir :**

Les **fromages de chèvres** de la chèvre de Milo, attention vente du nouveau **fromage à l'ortie** juste pour le marché !
 
Les **œufs** d'Angélique (Zeubio) ! 
 
Le **miel** de Cécile (Au Rythme des Abeilles) ! 
 
Les **champignons** bio de ChampiMonts ! 
 
Les **pains bios**, avec le retour de nos boulangers préférés O' Pain d'Autrefois !
 
Les **légumes bio** de la Ferme du Petit Arbre !

La rumeur courait dans le village la semaine passée que le maraîcher aurait des courgettes... Cette même rumeur a tourné hourvari : on parle depuis mardi sur France Info d'un concours pour gagner le plus gros specimen de cucurbitaceae ~~du monde~~ de la Ferme du Petit Arbre. Info ou intox ?

Un café offert pour les grands enfants et petites crêpes pour les petits enfants ! 🥞☕️

--- 
<br/>

## Ensemble, construisons la charte du Marché Bonne Mine
Les producteurs du marché de Sourcieux-les-Mines bouillonnent d’idées !

Afin que le Marché Bonne Mine vous ressemble et nous rassemble encore mieux, qu’il continue d’évoluer vers un lieu riche et convivial, l’équipe souhaite rédiger une Charte d’engagements regroupant valeurs partagées et transparence.

Nous vous proposons donc de donner votre avis sur le Marché Bonne Mine et son évolution, en répondant à un petit questionnaire. Vous pouvez le remplir en [suivant ce lien](https://www.marchebonnemine.fr/charte/) ou directement au marché en version papier samedi matin !

---
<br/>

## Excellente fin de semaine à tous, à samedi !

