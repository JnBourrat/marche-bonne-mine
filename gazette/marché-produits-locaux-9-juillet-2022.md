---
title: Ce samedi 9 Juillet 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, cerises, porc fermier
description: Découvrez les producteurs présents sur place en ce samedi 9 juillet ! Direction l'Italie pour la recette de la semaine ! [...]
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 07/06/2022
---

**Vous trouverez ce samedi, de 9h à 12h :**

- **Saucisses**, **grillades**, **pâtés de campagne** et cætera venus tout droit du crêt d'Arjoux avec **Terre d'Arjoux** ! N'oubliez pas que vous pouvez passer votre <a href="https://www.terredarjoux.fr/nos-produits/" target="blank">commande sur internet</a> - jusqu'au mardi précédant le marché - si vous souhaitez récupérer votre panier de **viande de porc** déjà préparé auprès de **Benoît** !

- **Laura et Mickaël (La Chèvre de Milo)**, qui décidément n'en finissent pas de régaler la chique avec leurs **fromages de chèvre** !

- Les **légumes et aromatiques de La Ferme du Petit Arbre** ! Les variétés de **tomates anciennes** seront de la partie ! Qui a dit **cœur de bœuf** ? 😋

- Les **œufs bio d'Angélique (Zeubio)** seront cachés, comme pour Pâques, entre les tomates et les courgettes !

- Sans pour autant pallier son absence, les **champignons déshydratés** (pleurotes et shiitakés) et les **soupes** de **Paul (ChampiMonts)** seront disponibles au magasin !

- Le **miel de Cécile (Au Rythme des Abeilles)** sera également disponible au magasin ! Ne ratez pas le miel d'**Acacia 2022** !



---
<br/>

## L'instant recette
<br/>

Cette semaine direction le soleil de l'Italie ! Cette fois-ci la recette contient des oeufs, des **tomates**, du **fromage de chèvre**, des **courgettes** et des **champignons** : une <a href="https://www.colruyt.be/fr/en-cuisine/recette/champignons-farcis-au-chevre" target="blank">bonne frittata à la tomate, courgette et fromage de chèvre</a> ! Petite astuce carnée pour parachever le plat : ajoutez quelques **lardons** ! C'est bon les lardons !

<br/>
<br/>

---
<br/>

## L'équipe du MBM vous souhaite une très très (très) bonne fin de semaine !
<br/>

<img src="/equipe-MBM.jpg" alt="les producteurs du Marché Bonne Mine" title="les producteurs du Marché Bonne Mine" height="auto">
 
_**« Le plaisir se ramasse, la joie se cueille et le bonheur se cultive. » - Bouddha** (à sa mère adoptive Mahaprajapati Gautami quand elle lui demandait d'aller récolter les tomates pour le déjeuner tandis que lui préférait lancer des débats philosophiques interminables parce que faut pas se leurrer, Bouddha était un fainéant de première)_

<br/>

---
<br/>

## À SAMEDI !