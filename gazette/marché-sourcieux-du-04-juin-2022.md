---
title: Ce samedi 04 Juin 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, cerises, fraises
description: Le premier Marché Bonne Mine du mois de Juin 2022 arrive à grands pas ! Découvrez les producteurs présents sur place en ce samedi 04 juin ! Ainsi qu'une information sur La Caserne, nouveau tiers-lieu culturel et social à Sourcieux-les-Mines !
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 06/01/2022
---

**Comme toutes les semaines, voici la liste des produits présents au marché :**

- La salade de pleurotes ? Un véritable ébaudissement culinaire ! Venez rencontrer **Paul (ChampiMonts)** et ses **champignons** !
 
- **Laetitia (Aux P'tits Plaisirs des Champs)** ramènera une nouvelle fois ses **fraises** (entre autres délices) !

- Les **cerises de Claire (La Terre Mère)** seront également de la partie !

- Les **œufs bio d'Angélique (Zeubio) !** 

- Le **miel de Cécile (Au Rythme des Abeilles)** ! 
 
- Les **pains d'O Pain d'Autrefois** ! 
 
- Les **légumes** et **aromatiques** de **La Ferme du Petit Arbre** !

- La star indétrônable du marché reste bien-entendu le **fromage de chèvre** de **Laura et Mickaël (La Chèvre de Milo)** !
 
--- 
<br/>

## LA CASERNE, UN TIERS-LIEU CULTUREL ET SOCIAL À SOURCIEUX-LES-MINES
<br/>
<img src="/bandeau-la-caserne.png" alt="logo de La Caserne" title="logo La Caserne" height="auto">
<br/>

Cette semaine, les brèves font place nette pour une **information de premier ordre**, sous le signe du partage et de la culture !

Un tiers-lieu culturel et social est en cours de création dans l'**ancienne caserne des pompiers** de Sourcieux-les-Mines ! Le collectif est actuellement en train de réunir les besoins, envies et idées de chacun, notamment via un questionnaire. Si vous ne savez pas ce qu'est un tiers-lieu c'est d'ailleurs très bien expliqué en préambule du formulaire !

Si vous souhaitez en savoir plus, rendez-vous sur leur page Facebook en suivant <a href="https://www.facebook.com/lacasernesourcieux" target="\_blank">ce lien</a> !

Et oui, le mot de la semaine est **tiers-lieu**, et non **ébaudissement**.

---
<br/>

## Vous êtes toujours aussi nombreux à venir nous voir, merci pour votre soutien ! À samediiiiiiiiiiiiii !