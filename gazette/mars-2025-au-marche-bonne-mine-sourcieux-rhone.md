---
title: Mars 2025 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, porc fermier
description: Retrouvez le calendrier du mois de mars 2025 au Marché Bonne Mine !
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: 2503-apercu.png
date: 02/28/2025
---
<br/>

<img class="planning-gazette aligne-a-droite"
   src="/2503-numerique.png" 
   alt="Affiche du marché Bonne Mine de mars. Marché ouvert tous les samedis de 9h à 12h au GAEC La Chèvre de Milo, Sourcieux-les-Mines. Adresse : 142, route du Cervais 69210 Sourcieux-les-Mines. Produits disponibles en vente directe : - Fromages de chèvre - La Chèvre de Milo (AB, Sourcieux-les-Mines) - Légumes - La Ferme du Petit Arbre (AB, Savigny) - Pain - O Pain d'Autrefois (Sourcieux-les-Mines) - Œufs - Zeubio (AB, Saint-Julien-sur-Bibost) - Miel - Au rythme des abeilles (AB, Châtillon) - Pommes - Le verger des Presles (AB, Pollionnay) Événements spéciaux : - Samedi 1er mars : Champignons, tempeh, farine - Champimonts du Lyonnais (AB, Villechenève) - Samedi 15 mars (3ème samedi du mois, artisanat) :   - Affûteur itinérant - FX Chastang   - Confitures, gelées - Les Cuillères d'Eddy (récoltes sauvages)   - Saint-Nectaire fermier - GAEC Les Moussages (AB, Labessette)   - Champignons, tempeh, farine - Champimonts du Lyonnais (AB, Villechenève) - Samedi 29 mars :   - Vin rouge & blanc - Martin Thiberge, vigneron furtif (AB, Ternand)   - Œufs, savons - Zeubio (AB, Saint-Julien-sur-Bibost)   - Champignons, tempeh, farine - Champimonts du Lyonnais (AB, Villechenève)   - Fruits, jus - Le verger des Presles (AB, Pollionnay) Contact : contact@marchebonnemine.fr Facebook : facebook.com/marchebonnemine",
   title="Calendrier mars 2025 au Marché Bonne Mine">
<br/>
