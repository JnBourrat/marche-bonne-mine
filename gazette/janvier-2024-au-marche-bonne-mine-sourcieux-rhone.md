---
title: Janvier 2024 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, porc fermier
description: L'année 2024 commence avec un joli mois de janvier au Marché Bonne Mine. Au programme de ce début d'anné[...]
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: apercu-planning-2401.png
date: 01/01/2024
---
<br/>

<img class="planning-gazette aligne-a-droite"
   src="/planning-complet-2401.png" 
   alt="Planning de janvier 2024 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69). 142, route du gervais, 69210 Sourcieux-les-Mines.
          En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny), Champignons bio & soupes - Champimonts du Lyonnais (AB, Villechenève).
          En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs bio - Zeubio (AB, Saint-Julien-sur-Bibost), Miel bio - Au rythme des abeilles (AB, Châtillon).
          + LE SAMEDI 13 JANVIER : Bières artisanales - Prisca, Confitures & gelées issues de récoltes sauvages - Les cueillettes d'Eddy.
          + LE SAMEDI 20 JANVIER, 3ÈME SAMEDI DU MOIS CONSACRÉ À L'ARTISANAT : Affûteur itinérant - FX Chastang, Réparation de vélo sur place - SW Cycles (Saint-Pierre-la-Palud), Saint-Nectaire fermier bio - GAEC des Moussages.
          + LE SAMEDI 27 JANVIER  DERNIER SAMEDI DU MOIS - GRAND MARCHÉ : Saint-Nectaire fermier bio - GAEC des Moussages, confitures de cueillettes sauvages - Les cueillettes d'Eddy, Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny), Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost), Œufs, savons - Zeubio (AB, Saint-Julien-sur-Bibost), Fruits, jus - Le verger des Presles (AB, Pollionay), Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères).
          + Réservez lapins, volailles & steaks hachés pour le samedi 3 février au 06 79 65 02 34 - Aux p'tits plaisir des champs (Savigny)!"
   title="Planning janvier 2024 au Marché Bonne Mine">
<br/>
