---
title: Mars 2024 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, paysan, biologique, paysan, fromage, légumes, champignons, miel, oeufs, pain, porc fermier
description: Le beau temps arrive avec le mois de Marsau Marché Bonne Mine. Au programme de ce mois, encore pleins de bons produits locau...
tags: marché de producteurs, Sourcieux-les-Mines, Marché Bonne Mine, Rhône, paysan
image: 2403-apercu.png
date: 03/01/2024
---
<br/>

<img class="planning-gazette aligne-a-droite"
   src="/2403-numerique.png" 
   altText="Planning de mars 2024 du Marché Bonne Mine : marché paysan tous les samedi de 9h à 12h au GAEC La Chèvre de MiLo, à Sourcieux-les-Mines (69). 142, route du gervais, 69210 Sourcieux-les-Mines. En vente directe producteur·rice tous les samedis : Fromages de chèvre - La Chèvre de MiLo (Sourcieux-les-Mines), Légumes bio - La Ferme du Petit Arbre (AB, Savigny), Champignons bio & soupes - Champimonts du Lyonnais (AB, Villechenève). En dépôt tous les samedis également : Pain - O Pain d’Autrefois (Sourcieux-les-Mines), Œufs bio - Zeubio (AB, Saint-Julien-sur-Bibost), Miel bio - Au rythme des abeilles (AB, Châtillon). + LE SAMEDI 9 MARS : lapins et volailles en direct de la ferme - Aux p'tits plaisir des champs (Savigny), Bières artisanales Prisca (Sain Bel), confitures et gelées issues de récoltes sauvages 'Les cueillettes d\'Eddy', Saint-Nectaire fermier bio - GAEC des Moussages. + LE SAMEDI 16 MARS, 3ÈME SAMEDI DU MOIS CONSACRÉ À L'ARTISANAT : Affûteur itinérant - FX Chastang, Réparation de vélo sur place - SW Cycles (Saint-Pierre-la-Palud). + LE SAMEDI 23 MARS Vin rouge, blanc, rosé - Le Domaine du Tane (AB, Saint-Étienne-des-Ouillères). + LE SAMEDI 30 MARS  DERNIER SAMEDI DU MOIS - GRAND MARCHÉ : Les Serres de Ripan - Pépinière de plantes potagères & aromatiques, Tisanes, phytothérapie, huiles de soin - La Terre Mère (AB, Savigny), Viande de porc plein air - Terre d’Arjoux (AB, Saint-Julien-sur-Bibost), Œufs, savons - Zeubio (AB, Saint-Julien-sur-Bibost). + PLEIN D’AUTRES SURPRISES !"
   title="Planning du mois de mars 2024 au Marché Bonne Mine (Sourcieux-les-Mines).">
<br/>
