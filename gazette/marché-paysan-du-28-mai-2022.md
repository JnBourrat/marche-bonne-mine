---
title: Ce samedi 28 Mai 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, cerises, viande, porc, fermier, plein air, tomme aux orties, savon, fraises, fruits
description: "Le dernier samedi de chaque mois, l'offre s'étoffe au Marché Bonne Mine ! Ce week-end nous aurons donc des légumes, du miel, du fromage, des champignons, des œufs, du savon, du pain, de la viande de porc, des cerises et des fraises ! Partez à la cueillette d'informations en lisant l'article."
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 05/25/2022
---

**Ça y est ! Le dernier samedi du joli (trop joli ?) mois de Mai arrive ! Et il arrive solidement escorté par vos producteurs locaux préférés :**

- **Laetitia (Aux P'tits Plaisirs des Champs)** avec des **fraises** !

- **Olivier & Benoît (Terre d'Arjoux)**, avec leur **viande de porc bio & plein air** (grillades, saucisson, jambon cuit, lardons, pâté de campagne, sauté, rôti...)

- **Claire (La Terre Mère)** et ses **cerises** !

- **Angélique (Zeubio)**, accompagnée de ses **œufs** et **savons** ! 

- **Cécile (Au Rythme des Abeilles)** avec son miel ! 
 
- **Paul (ChampiMonts)** et ses champignons  ! 
 
- Une fois n'est pas coutume, le **pain** proviendra ce week-end de la **Boulangerie Jacoud**, située à Savigny !
 
- **Jr & Jn (La Ferme du Petit Arbre)** avec des caisses et des caisses de **légumes** et **aromatiques** !

- **Laura et Mickaël (La Chèvre de Milo)** se fendent encore d'exclusivités en cette fin de semaine ! Parmi leurs **fromages de chèvre** vous retrouverez également quelques **saucissons de bique**, de la **tomme aux orties** ainsi qu'une surprise ! Surprise que l'on ne vous divulgâchera pas dans cette infolettre...

--- 
<br/>

## Brèves de marché

- Si vous ne l'aviez pas noté, le dernier samedi de chaque mois, l'offre s'étoffe au marché de producteurs de Sourcieux-les-Mines ! Ce week-end nous aurons donc des **légumes**, du **miel**, du **fromage**, des **champignons**, des **œufs**, du **savon**, du **pain**, de la **viande de porc**, des **cerises** et des **fraises** !
 
- Nous le savons, certains d'entre vous ont prévu de profiter d'un pont bien mérité afin de passer un week-end prolongé en Drôme Provençale. Nous savons aussi que vous serez tenté de l'annuler à l'idée de rater le Marché Bonne Mine... Qu'à cela ne tienne ! Envoyez-nous votre voisin, tante, grand-père et autre nièce avec votre liste de course ! Car en plus de faire découvrir le marché à une nouvelle personne, celle-ci profitera certainement d'une bonne balade dans Sourcieux pour bien commencer le week-end !
 
- Bien vu, lecteur assidu : le mot(-valise) de la semaine était « divulgâcher ». 

---
<br/>

## À samedi, si le cœur vous en dit ! ©

_© Copyright Nicolas Stoufflet, « Le Jeu des 1000 euros », du lundi au vendredi à 12h45 sur France Inter_