---
title: Ensemble, construisons la charte du Marché Bonne Mine !
category: charte, engagements, marché, producteurs
description: Afin que le Marché Bonne Mine vous ressemble et nous rassemble encore mieux, qu’il continue d’évoluer vers un lieu riche et convivial, l’équipe souhaite rédiger une Charte d’engagements regroupant valeurs partagées et transparence !
tags: marché, charte
date: 05/02/2022
---

Les producteurs du marché de Sourcieux-les-Mines bouillonnent d’idées !

Afin que le Marché Bonne Mine vous ressemble et nous rassemble encore mieux, qu’il continue d’évoluer vers un lieu riche et convivial, l’équipe souhaite rédiger une Charte d’engagements regroupant valeurs partagées et transparence.

Nous vous proposons donc de donner votre avis sur le Marché Bonne Mine et son évolution, en répondant à un petit questionnaire. Vous pouvez le remplir **[en suivant ce lien](https://www.marchebonnemine.fr/charte)** ou directement au marché en version papier !

À très vite !
