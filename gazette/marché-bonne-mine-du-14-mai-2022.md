---
title: Ce samedi 14 Mai 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, charte, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain
description: Des oeufs, du pain, des légumes, du miel, du fromage de chèvre, des champignons... Cliquez pour savoir tout ce que vous réserve le Marché Bonne Mine du samedi 14 mai 2022 !
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 05/11/2022
---

**Vous retrouverez les :**

- **Esculents œufs** d'Angélique (Zeubio) ! 
 
- **Délicieux pots de miel** de Cécile (Au Rythme des Abeilles) ! 
 
- **Fameux champignons** de Paul (ChampiMonts) ! 
 
- **Croustillants pains** d'O Pain d'Autrefois !
 
- **Appétissants légumes** de la Ferme du Petit Arbre !

- **Savoureux fromages de chèvre** de la chèvre de Milo ! En exclusivité pour le marché, Laura et Mickaël vous préparent une ribambelle d'apéros aux goûts aussi diversifiés que succulents : ail, paprika, ail des ours et herbes du jardin. Nous vous conseillons tout de même de conserver un petit bout de pain pour la cervelle de canut.

--- 
<br/>

## Quelques nouvelles en vrac, comme les oignons

**I.** Si vous ne souhaitez pas attendre le dernier samedi de chaque mois pour vous délecter de la viande de porc de Terre d'Arjoux, vous pouvez passer commande sur leur site en suivant <a href="https://www.terredarjoux.fr/nos-produits/" target="\_blank">ce lien</a>. N'oubliez pas de préciser que vous souhaitez retirer votre commande au Marché Bonne Mine en fin de formulaire.

**II.** Restons dans le numérique. Si vous souhaitez voir ce que la Ferme du Petit Arbre aura à vous proposer -dans la limite des stocks disponibles imposés par Dame Nature- rendez-vous sur son site internet en suivant <a href="https://www.la-ferme-du-petit-arbre.fr/boutique" target="\_blank">ce lien</a> !
 
**III.** Le concours du jeu "Devinez le poids de la courgette" a été gagné par Élodie. Un instant magique que seuls les grands gagnants du Loto peuvent comprendre... Je cite la lauréate : « Qui aurait pensé qu'une courgette allait changer ma vie ? »

**IV.** Vous noterez que si le mot de la semaine passée était _hourvari_, cette semaine il s'agit du mot _esculent_ !
 
**V.** Vous avez encore quelques jours pour donner votre avis sur la constitution de la charte du Marché Bonne Mine et son évolution, en répondant à un petit questionnaire en suivant [ce lien](https://www.marchebonnemine.fr/charte/).

---
<br/>

## Restez au frais, à samedi !

