---
title: Marché Bonne Mine en fête II - Samedi 30 septembre 2023
category: marché, producteurs, conférence gesticulée, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, porc fermier, fête, vin chaud, bière chaude, jus de pomme chaud, buvette
description: Un grand marché le matin, une conférence gesticulée à 17h et un bal folk à 21h ! Retrouvez le program[...]
tags: marché, conférence gesticulée, bal folk, Sourcieux-les-Mines, Marché Bonne Mine, Rhône
image: affiche-mbm-en-fete-2-mini.jpg
date: 09/18/2023
---
<br/>
<img class="affiche-evenement" src="/affiche-mbm-en-fete-2.jpg" alt="affiche pour la soirée du Marché Bonne Mine du 30 septembre 2023" title="affiche marché de noël">
<br/>

### UN GRAND MARCHÉ LE MATIN, UNE CONFÉRENCE GESTICULÉE À 17H & UN BAL FOLK À 21H !
#### Retrouvez le programme de la seconde édition du Marché Bonne Mine En Fête !
<br>

- **9H - 12H : MARCHÉ « PAYSANNERIE & ARTISANAT »**
 
- **16 H : ASSEMBLÉE GÉNÉRALE PUBLIQUE DU MARCHÉ BONNE MINE**

- **17H : « DE LA FOURCHE À LA FOURCHETTE... NON ! L'INVERSE !! » AVEC MATHIEU DALMAIS**

   **Conférence gesticulée sur la sécurité sociale de l'alimentation** suivie d'un débat. 

   **Durée** : 1h30 à 1h45
   
   **Publics** : Tout public à partir de 15 ans, dès lors qu'il y a un intérêt pour la transformation de notre agriculture, la lutte pour le droit à l'alimentation et/ou le projet de Sécurité sociale de l'alimentation ! <a href="https://conferences-gesticulees.net/conferences/de-fourche-a-fourchette-non-linverse/" target="blank">Cliquez ici pour en savoir plus sur la conférence</a>

 
- 1**9H30 : REPAS PAR BABAELLYA TRUCK & BUVETTE**
   Au menu : burger / frites ou bien wrap végétarien / frites !
 
- **21H : BAL FOLK AVEC LE DUO CAILINI**

   Pour clore la journée en musique, nous accueillerons une nouvelle fois <a href="https://www.facebook.com/DuoCailini" target="blank">Cailini</a>, duo Folk composé de Sandrine Saliba (accordéon) et Estelle Avondo (alto), qui vous fera danser au gré de rythmes folk !
<br>
