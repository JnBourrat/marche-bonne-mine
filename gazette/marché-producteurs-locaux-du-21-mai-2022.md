---
title: Ce samedi 21 Mai 2022 au Marché Bonne Mine
category: marché, producteurs, local, Sourcieux-les-Mines, Sourcieux, agriculture biologique, charte, paysan, fromage, légumes, maraîchage, champignons, miel, oeufs, pain, cerises
description: "Les cerises arrivent ! Avec elles : des oeufs, du pain, des légumes, du miel, du fromage de chèvre, des champignons. Ouvrez l'article si vous souhaitez tout savoir des surprises que vous réserve le Marché Bonne Mine pour ce samedi 21 mai 2022 !"
tags: marché, producteurs, local, Sourcieux-les-Mines, paysan
date: 05/18/2022
---

**Vous retrouverez :**

- **Les Cerises** de Claire ! Ça y est, la saison des Burlats commence ! Nous vous conseillons de venir tôt car le maraîcher est un véritable dévoreur de cerises devant l'éternel ! Il ne pourra pas s'empêcher d'en manger par poignées ! Et il a de grandes mains...

- **Les œufs** d'Angélique ! 

- **Le miel** de Cécile ! 
 
- **Les champignons** de ChampiMonts ! 
 
- **Les pains** d'O Pain d'Autrefois  !
 
- **Les légumes** de la Ferme du Petit Arbre !

- **Les fromages** de chèvre de la chèvre de Milo !

--- 
<br/>

## Brèves et autre joyeusetés

- Merci aux 2045 personnes (nombre d'habitants de Sourcieux-les-Mines d'après le dernier recensement INSEE) qui ont répondu au questionnaire sur la **charte du Marché** ! Nous prenons en compte vos remarques et ferons de notre mieux pour répondre à vos envies et besoins !
 
- Finissons cette infolettre sur un joli proverbe, qui ne sous-tend aucune prise de position vis-à-vis des professionnels de santé :


**« Si toute l'année il y avait des cerises, Messieurs les médecins n'iraient plus qu'en chemise. »**

---
<br/>

## À samedi !

_P-S. : Si vous l'aviez manqué, le mot de la semaine était naturellement « infolettre » et non « cerises »_