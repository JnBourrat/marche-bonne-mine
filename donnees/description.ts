import producteurs from './producteurs';

const premierePhrase: string =
  'Retrouvez-nous tous les samedis de 9h00 à 12h00 au Gaec La Chèvre de Milo à Sourcieux-les-Mines (Rhône) ! Venez à la rencontre de vos producteurs, artisans et paysans locaux. Suivant les semaines, et en fonction des saisons, vous pourrez trouver différents types de produits : ';

const produits: string =
  producteurs
    .map((producteur) => producteur.typeDeProduits.toString())
    .toString()
    .replaceAll(',', ', ') + '...';

const description = premierePhrase + produits;

export default description;
