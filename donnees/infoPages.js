const infoPages = {
  default:
    'Marché Bonne Mine à Sourcieux-les-Mines (69), au Gaec La Chèvre de Milo. Marché paysan à la ferme tous les samedis de 9h00 à 12h00 : vente direct producteur de fromages, oeufs, champignons, miel, légumes, porc fermier, tisanes, herbes, vin, pain.',
  contact:
    'Marché paysan, 142 Route du Gervais, 69210 Sourcieux-les-Mines. 100% Paysan, local & convivial ! Marché Bonne Mine. Marché Bio Sourcieux.',
  producteurs:
    "Marché de producteurs à Sourcieux-les-Mines : le Domaine du Tane, la Ferme du Petit Arbre, La Chèvre de Milo, ChampiMonts, Zeubio, La Terre Mère, Au Rythme des Abeilles, O Pain d'Autrefois.",
  mainTitle: 'Marché Bonne Mine',
  charte: 'Charte des producteurs du Marché Bonne Mine en co-construction avec les commerçants.',
  fete: "C'est la fête au Marché Bonne Mine ! Marché producteurs et artisans locaux, catch d'improvisation théâtrale, repas midi & soir, concert de jazz & buvette seront au rendez-vous !",
};

export default infoPages;
