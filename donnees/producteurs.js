export const producteursOrdonnes = [
  {
    image: '/fermedupetitarbre.png',
    nom: 'La Ferme du Petit Arbre',
    adresse: 'Savigny',
    site: 'https://www.la-ferme-du-petit-arbre.fr',
    facebook: 'https://www.facebook.com/people/La-Ferme-du-Petit-Arbre/61555684930112',
    description:
      "Jean-René vous propose jusqu'à 40 variétés de légumes suivant la saison : tomates, salades, courgettes, aubergine, épinards, choux, courges, navets et autres carottes.",
    typeDeProduits: ['légumes'],
  },
  {
    image: '/chevredemilo.png',
    nom: 'La Chèvre de Milo',
    adresse: 'Sourcieux-les-Mines',
    site: 'http://lachevredemilo.fr/',
    facebook: 'https://www.facebook.com/LaChevredeMiLo/',
    description:
      'Accompagnés de leurs chèvres, Mickaël et Laura vous accueillent dans leur ferme où sont produits et vendus de délicieux fromages fermiers.',
    typeDeProduits: ['fromages'],
  },
  {
    image: '/laterremere.jpg',
    nom: 'La Terre Mère',
    adresse: 'Savigny',
    description:
      "Claire produit à Savigny des cerises en agriculture biologique mais surtout des plantes médicinales qu'elle transforme et vous propose en tisane, huile de soin, teinture mère, hydrolat et gemmothérapie",
    typeDeProduits: ['plantes médicinales', 'tisanes', 'cerises'],
  },
  {
    image: '/terredarjoux.jpg',
    nom: "Terre d'Arjoux",
    adresse: 'Saint-Julien-sur-Bibost',
    site: 'https://www.terredarjoux.fr/',
    facebook: 'https://www.facebook.com/Terredarjoux/',
    instagram: 'https://www.instagram.com/terredarjoux/',
    description:
      'Olivier et Benoît vous proposent du porc né et élevé en plein air : grillades, saucisson, jambon cuit, lardons, pâté de campagne, sauté et rôti de porc, etc.',
    typeDeProduits: ['porc fermier'],
  },
  {
    image: '/champimonts.png',
    nom: 'ChampiMonts du Lyonnais',
    adresse: 'Villechenève',
    site: 'https://champimonts.fr/',
    twitter: 'https://twitter.com/champimonts',
    instagram: 'https://www.instagram.com/champimonts/',
    facebook: 'https://www.facebook.com/ChampiMonts',
    description:
      'Paul vous propose des champignons frais ou déshydratés. Des shiitakés et des pleurotes venus tout droit des Monts du Lyonnais !',
    typeDeProduits: ['champignons'],
  },
  {
    image: '/rythme_abeilles.png',
    nom: 'Au rythme des Abeilles',
    adresse: "Châtillon d'Azergues",
    site: 'http://aurythmedesabeilles.fr/',
    facebook:
      'https://www.facebook.com/pages/category/Agriculture/Au-rythme-des-abeilles-100966018373042/',
    description:
      'Cécile vous propose différents types de miel : toutes fleurs, chataîgnier, acacia, montagne, lavande, bruyère.',
    typeDeProduits: ['miel'],
  },
  {
    image: '/zeubio.png',
    nom: 'Zeubio',
    adresse: 'Saint-Julien-sur-Bibost',
    description:
      'Angélique vous propose des œufs de poules élevées au grand air et en plein bonheur à Saint-Julien-sur-Bibost, ainsi que des savons aux œufs.',
    typeDeProduits: ['oeufs'],
  },
  {
    image: '/domainedutane.jpeg',
    nom: 'Domaine du Tane',
    adresse: 'Saint-Étienne-des-Oullières',
    site: 'https://www.domainedutane.fr/',
    description:
      'Jacques vous propose du vin issu de son vignoble au pied de la colline de Brouilly. Une gamme de Beaujolais vinifiés à partir de l’unique cépage Gamay : Beaujolais Villages Nouveau, Beaujolais Villages, Beaujolais Villages Rosé, Brouilly, Coteaux Bourguignon, Perles de Gamays.',
    typeDeProduits: ['vin'],
  },
  {
    image: '/opaindautrefois-sm.jpg',
    nom: "O Pain d'Autrefois",
    adresse: 'Sourcieux-les-Mines',
    facebook: 'https://www.facebook.com/O-Pain-dAutrefois-671304693058921/',
    description:
      'Olivier, boulanger à Sourcieux propose... du pain ! Pain classiques ou spéciaux, il y en a pour tous les goûts !',
    typeDeProduits: ['pain'],
  },
  {
    image: '/cueillettes-eddy.jpg',
    nom: "Les cueillettes d'Eddy",
    adresse: 'Claveisolles',
    facebook: 'https://www.facebook.com/cueilletteddy/',
    site: 'https://www.cueilletteddy.com/',
    description:
      'Eddy vous propose de délicieuses confitures & gelées issues de ses diverses récoltes sauvages !',
    typeDeProduits: ['confitures', 'gelées'],
  },
  {
    image: '/verger-des-presles.jpg',
    nom: 'Le verger des Presles',
    adresse: 'Pollionay',
    description:
      'Pierrick propose sa production de fruits de saison (pommes, poires, kiwis, amandes...) bio ! Confitures, sirops ou vinaigre sont également de la partie !',
    typeDeProduits: ['fruits', 'jus de fruit', 'sirop', 'vinaigre de fruits'],
  },
  {
    image: '/ptits-plaisirs-des-champs.jpeg',
    nom: "Aux p'tits plaisirs des champs",
    facebook: 'https://www.facebook.com/Aux-ptits-plaisirs-des-champs-100077903550895',
    adresse: 'Savigny',
    description:
      'Laëtitia prend des commandes de lapins fermier, volailles et steaks hâchés de boeuf bio au 06 79 65 02 34. Elle passe au marché environ une fois par mois accompagnée de fruits et légumes de sa production afin de livrer les commandes de viande !',
    typeDeProduits: ['lapin', 'volaille', 'steak hâché de boeuf bio', 'fruits', 'légumes'],
  },
];

export const shuffleProducteurs = () =>
  producteursOrdonnes
    .map((value) => ({ value, sort: Math.random() }))
    .sort((a, b) => a.sort - b.sort)
    .map(({ value }) => value);

export default producteursOrdonnes;
