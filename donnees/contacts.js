const contacts = [
  {
    methode: 'Mail',
    valeur: 'contact@marchebonnemine.fr',
  },
  {
    methode: 'Facebook',
    valeur: 'https://www.facebook.com/marchebonnemine/',
  },
  {
    methode: 'Adresse',
    valeur: '142 Route du Gervais, 69210 Sourcieux-les-Mines',
    googleMaps:
      'https://www.google.com/maps/place/March%C3%A9+Bonne+Mine/@45.8068865,4.6169377,17z/data=!4m6!3m5!1s0x47f4f59498cdbe65:0xce0c4112c6f006d0!8m2!3d45.8065388!4d4.6148456!16s%2Fg%2F11t0p9xjpl?entry=ttu',
  },
];

export default contacts;
