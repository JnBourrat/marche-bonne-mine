# Site du Marché Bonne Mine ![Statut de déploiement Netlify](https://api.netlify.com/api/v1/badges/48c59f80-a3a8-4087-8c3c-88a143da192b/deploy-status)

## Pour contribuer

### Pré-requis logiciels

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [yarn](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable)

### Installation

Clônez le projet : `$ git clone https://framagit.org/JnBourrat/marche-bonne-mine.git`

Installez les dépendances : `$ yarn`

### Démarrage

Lancez le projet : `$ yarn dev`

---

## Fabriqué avec

- [Nuxt](https://nuxtjs.org/)
- [Vuetify](https://vuetifyjs.com/en/)

---

## Build et déploiement automatique des sources sur <https://marchebonnemine.fr/>

### Intégration continue et Déploiement continu - [Gitlab CI/CD](https://framagit.org/JnBourrat/marche-bonne-mine/-/blob/main/.gitlab-ci.yml)

Lorsqu'un changement est apporté dans la branche `main` ou à destination de cette branche (une Pull request par exemple), l'action 'continuous-integration' pour "Intégration Continue" se lance automatiquement afin de :

- lancer l'installation des dépendances : `npm i`
- vérifier la conformité et le respect des bonnes pratiques dans le code : `npm run lint`

**Déploiement continu** : un déploiement est effectué **automatiquement** à chaque changement (push ou merge request) sur la branche `main` de ce dépôt git.

## Mise à jour des billets par les administrateurs

Afin de faciliter la gestion de contenu sans alourdir le site par un portail gestionnaire, la solution d'alimentation des "billets" via framagit a été choisie pour le moment. À voir si suffisant sur le moyen terme.
Procédure :

- Créer une branche portant le nom du billet avec `gazette/` en préfixe (par exemple un titre mignon comme `gazette/les-carottes-sont-de-sorties`) ;
- Dans cette branche, créer un nouveau fichier dans le dossier **[gazette](/gazette)** nommé **"les-carottes-sont-de-sorties.md". `*.md`** étant l'extension des fichiers markdown ;
- Toute image devant être affichée dans le billet doit être la plus légère possible et ajoutée au dossier `static` ;
- Une fois le billet écrit (et visualisé sur framagit), une Merge Request (Demande de Changement) peut être effectuée depuis la branche `gazette/les-carottes-sont-de-sorties` vers la branche principale du dépôt : `main` ;
- Cette MR devra ensuite être revue minutieusement puis acceptée si elle ne comporte pas d'autres changement que l'ajout de fichiers md dans le dossier `gazette` ou d'images dans le dossier `static` ;
- Une fois validée, le site sera automatiquement mis à jour en quelques minutes.
