import path from 'path';
import glob from 'glob';
import colors from 'vuetify/es5/util/colors';
import infoPages from './donnees/infoPages.js';

const getDynamicRoutes = () => {
  return [].concat(
    glob
      .sync('*.md', { cwd: 'gazette/' })
      .map((filepath) => `/gazette/${path.basename(filepath, '.md')}`)
  );
};

const dynamicPaths = getDynamicRoutes();

export default {
  // Target (https://go.nuxtjs.dev/config-target)
  target: 'static',

  generate: {
    routes: dynamicPaths,
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s',
    title: infoPages.mainTitle,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    htmlAttrs: {
      lang: 'fr',
    },
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/main.scss', '@mdi/font/css/materialdesignicons.css'],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@/modules/generator'
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ['@nuxtjs/sitemap', '@nuxtjs/onesignal', '@nuxtjs/pwa', '@nuxtjs/robots'],

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    defaultAssets: false,
    treeShake: true,
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          anchor: colors.teal.lighten3,
        },
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    extend(config, ctx) {
      config.module.rules.push({
        test: /\.md$/,
        include: [path.resolve(__dirname, 'gazette'), path.resolve(__dirname, 'donnees')],
        loader: 'frontmatter-markdown-loader',
      });
      if (ctx && ctx.isClient) {
        config.optimization.splitChunks.maxSize = 51200;
      }
    },
  },
  router: {
    base: '/',
    extendRoutes(routes) {
      routes.push({
        name: 'default-accueil',
        path: '/',
        component: '~/pages/accueil.vue',
      });
    },
  },
  sitemap: {
    hostname: 'https://www.marchebonnemine.fr',
    gzip: true,
    exclude: [],
    defaults: {
      changefreq: 'daily',
      priority: 1,
      lastmod: new Date(),
    },
    trailingSlash: true
  },
  oneSignal: {
    init: {
      appId: '4520aa71-d0c5-4360-8cb6-003893f30d74',
      allowLocalhostAsSecureOrigin: true,
      welcomeNotification: {
          disable: true
      },
    },
  },
  pwa: {
    meta: {
      title: infoPages.mainTitle,
      author: 'jnbourrat.dev@gmail.com',
    },
    manifest: {
      name: infoPages.mainTitle,
      short_name: infoPages.mainTitle,
      lang: 'fr',
      description:
        'Application pour le Marché Bonne Mine, vente direct producteurs à Sourcieux-les-Mines (Rhône)',
      start_url: '/accueil',
      background_color: '#089b9b',
      theme_color: '#089b9b',
    },
    icon: {
      sizes: [192, 512],
      purpose: 'any',
    },
  },
  robots: {
    UserAgent: '*',
    Disallow: ''
  },
};
